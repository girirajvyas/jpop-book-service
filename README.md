# Jpop Book Service  

**Dependencies used:**  


**H2 DB configurations**  



**JPARepository VS PagingAndSortingRepository VS CRUDRepository**  


**Versioning in Rest:**  
/api/v1  
/api/v2  

**Advantage:**  
clients can switch version easily according to their will  

**Disadvantage:**  
code duplication for each version  

**References:**  
Code formatting - https://github.com/HPI-Information-Systems/Metanome/wiki/Installing-the-google-styleguide-settings-in-intellij-and-eclipse  
Configure in memory DB - https://www.baeldung.com/spring-boot-h2-database  
Configure Swagger -   
https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api  
https://dzone.com/articles/spring-boot-2-restful-api-documentation-with-swagg  
Swagger Bug: https://github.com/springfox/springfox/issues/2265#issuecomment-413286451
 
  
aop - https://o7planning.org/en/10257/java-aspect-oriented-programming-tutorial-with-aspectj  

#Eureka client
1. Add @EnableEurekaClient (need to add dependency in pom before this)  
2. Add "eureka.client.service-url.default-zone=http://localhost:8761" in application.properties  
3. In case you do not add "spring.application.name=book-service", your service will be registered with 'UNKNOWN' name  
4. Add bottstrap.properties and add the name property in it