package com.epam.jpop.bookservice;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.epam.jpop.bookservice.service.BookService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookServiceApplicationTests {
  
  @Autowired
  private BookService bookService;

  @Test
  public void contextLoads() {
    assertThat(bookService).isNotNull();
  }

}
