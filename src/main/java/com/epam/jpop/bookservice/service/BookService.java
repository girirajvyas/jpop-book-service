package com.epam.jpop.bookservice.service;

import java.util.List;
import java.util.Optional;
import com.epam.jpop.bookservice.model.Book;

/**
 * Service to fetch data from repository
 * 
 * @author giri
 *
 */
public interface BookService {

  public List<Book> findAll();

  public Optional<Book> findById(Long id);

  public Book save(Book book);

  public void deletebyId(Long id);

}
