package com.epam.jpop.bookservice.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.jpop.bookservice.model.Book;
import com.epam.jpop.bookservice.repository.BookRepository;

/**
 * 
 * @author giri
 *
 */
@Service
public class BookServiceImpl implements BookService {

  @Autowired
  BookRepository bookRepository;

  public List<Book> findAll() {
    return bookRepository.findAll();
  }

  public Optional<Book> findById(Long id) {
    return bookRepository.findById(id);
  }

  public Book save(Book book) {
    return bookRepository.save(book);
  }

  public void deletebyId(Long id) {
    bookRepository.deleteById(id);
  }

}
