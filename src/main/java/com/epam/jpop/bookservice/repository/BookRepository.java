package com.epam.jpop.bookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import com.epam.jpop.bookservice.model.Book;

/**
 * Book repository giving out of the box all the CRUD methods that will be required. We can also
 * extend {@link CrudRepository}
 * 
 * @author giri
 *
 */
public interface BookRepository extends JpaRepository<Book, Long> {

}
