package com.epam.jpop.bookservice.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.epam.jpop.bookservice.model.Book;
import com.epam.jpop.bookservice.model.BookDto;
import com.epam.jpop.bookservice.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Books end-point having all the end-points related to the books
 * 
 * @author giri
 *
 */
@Api(value = "Book Service")
@RestController
@RequestMapping("/v1/books")
public class BooksController {

  @Autowired
  private BookService bookService;

  @Autowired
  private ModelMapper modelMapper;

  @ApiOperation(value = "Find all the books", response = List.class)
  @GetMapping()
  public List<BookDto> findAll(HttpServletRequest request) {
    String key = request.getHeader("API_KEY"); // verify if we get the key from feign client
    //System.out.println("******************** KEY: " + key);
    List<Book> books = bookService.findAll();
    return books.stream().map(book -> convertToDto(book)).collect(Collectors.toList());
  }

  @ApiOperation(value = "Get book by id", response = Book.class)
  @GetMapping("/{book_id}")
  public ResponseEntity<BookDto> findById(
      @ApiParam(value = "book id by which user will be retrieved",
          required = true) @PathVariable("book_id") long id) {

    Optional<Book> book = bookService.findById(id);
    if (!book.isPresent()) {
      return ResponseEntity.badRequest().build();
    }

    return ResponseEntity.ok(convertToDto(book.get()));
  }

  @ApiOperation(value = "Add a book", response = Book.class)
  @PostMapping
  public ResponseEntity<Book> create(@ApiParam(value = "Book object store in database table",
      required = true) @RequestBody BookDto bookDto) {
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(bookService.save(convertToEntity(bookDto)));
  }

  @ApiOperation(value = "update a book", response = Book.class)
  @PutMapping("/{book_id}")
  public ResponseEntity<Book> update(
      @ApiParam(value = "book id by which user will be retrieved",
          required = true) @PathVariable("book_id") long id,
      @ApiParam(value = "Book object store in database table",
          required = true) @RequestBody BookDto bookDto) {
    if (bookService.findById(id).isPresent()) {
      ResponseEntity.badRequest().build();
    }

    return ResponseEntity.ok().body(bookService.save(convertToEntity(bookDto)));
  }

  @ApiOperation(value = "delete a book", response = Book.class)
  @DeleteMapping("/{book_id}")
  public ResponseEntity<Book> delete(@ApiParam(value = "book id by which user will be retrieved",
      required = true) @PathVariable("book_id") long id) {
    if (bookService.findById(id).isPresent()) {
      ResponseEntity.badRequest().build();
    }

    bookService.deletebyId(id);
    return ResponseEntity.ok().build();
  }

  private Book convertToEntity(BookDto bookDto) {
    return modelMapper.map(bookDto, Book.class);
  }

  private BookDto convertToDto(Book book) {
    return modelMapper.map(book, BookDto.class);
  }
}
