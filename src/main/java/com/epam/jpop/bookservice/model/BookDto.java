package com.epam.jpop.bookservice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@ApiModel(description = "All details about the Book. ")
public class BookDto {

  @ApiModelProperty(notes = "The database generated ID")
  private Long id;
  
  @ApiModelProperty(notes = "Book name")
  private String name;
  
  @ApiModelProperty(notes = "Author of the book")
  private String author;
  
  @ApiModelProperty(notes = "Genre of the book")
  private String category;
  
  @ApiModelProperty(notes = "Short summary")
  private String description;
 
}
