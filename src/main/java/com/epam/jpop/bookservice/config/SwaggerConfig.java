package com.epam.jpop.bookservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Basic swagger configurations
 * <ol>
 * <li>Default: http://localhost:8080/v2/api-docs</li>
 * <li>With swagger-ui dependency: http://localhost:8080/swagger-ui.html</li>
 * </ol>
 * 
 * @author giri
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2).select() // builder instantiation
        .apis(RequestHandlerSelectors.basePackage("com.epam.jpop.bookservice.controller"))
        .paths(PathSelectors.any()) // end-point path can be defined here
        .build().apiInfo(apiEndPointsInfo());
  }

  private ApiInfo apiEndPointsInfo() {
    return new ApiInfoBuilder().title("Books REST API").description("")
        .contact(new Contact("Giriraj Vyas", "girirajvyas.github.io", "girirajvyas21@gmail.com"))
        .license("GNU public license").version("0.0.1-SNAPSHOT").build();
  }

  /**
   * Default implementation reference. It will have 4 end-points in documentation: < 1.
   * <ol>
   * <li>basic error controller</li>
   * <li>Book Service Controller</li>
   * <li>Operation Handler (actuator /health and /info )</li>
   * <li>Web Mvc Links Handler (/actuator)</li>
   * </ol>
   * 
   * As we need only the book controller customizing in another bean
   * 
   * @return
   */
  @SuppressWarnings("unused")
  private Docket api2() {
    return new Docket(DocumentationType.SWAGGER_2) // docType swagger 2
        .select() // builder instantiation
        .apis(RequestHandlerSelectors.any()) // package can be defined here
        .paths(PathSelectors.any()) // end-point path can be defined here
        .build();
  }
}
