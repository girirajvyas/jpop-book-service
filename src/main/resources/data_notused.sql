DROP TABLE IF EXISTS book;
 
CREATE TABLE book (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  author VARCHAR(100) NOT NULL,
  category VARCHAR(100) NOT NULL,
  description VARCHAR(250) DEFAULT NULL
);
 
INSERT INTO book (name, author, category, description) VALUES
  ('Two states', 'Chetan', 'Fiction', 'Love story'),
  ('Midnights children', 'Chetan', 'Fiction', 'Love story'),
  ('Alchemist', 'Chetan', 'Fiction', 'Love story');